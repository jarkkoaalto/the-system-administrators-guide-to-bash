#!/bin/bash

# simple file reading and displaying one line at a time

echo "Enter a filename to read: "

#DEBUG START
set -x # Start debugin here
read FILE

while read -r SUPERHERO;
do
	echo "Superhero Name: $SUPERHERO"
done < "$FILE"

set +x
# STOPS DEBUG
