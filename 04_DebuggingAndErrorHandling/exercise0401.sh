#!/bin/bash

# Demo of using error handling with exit

echo "Change to a directory and list the content"
DIRECTORY=$1

cd $DIRECTORY 2>/dev/null

if [ "$?" = "0" ]; then
	echo "We can change into the dirctory $DIRECTORY, and here the contents"
	echo "`ls -la`"
else
	echo "Cannot change directorties, existing with an error no listing"
	exit 1
fi
