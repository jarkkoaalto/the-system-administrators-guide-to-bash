#!/bin/bash
#demo of using error handlng with exit

echo "Change to a directory and list the contest"
DIRECTORY=$1

cd $DIRECTORY 2>/dev/null

# ls -al
# This is danger !!!!!!! nothing do not stop you use rm *-command

if [ "$?" = "0" ]; then
	echo "We can change into the directory $DIRECTORY, and here are the contents"
	echo "`ls -al`"
else
	echo "Cannot change directories, existing with an error and no listing"
	exit 1 ## You can create own error handling book. If you need it
fi
