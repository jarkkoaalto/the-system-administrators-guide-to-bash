#!/bin/bash

# Demo of a dialog box that will display a menu

# dialog variables / default values
MENUBOX=${MENUBOX=dialog}

# function decaltations - start

# function to display a simple menu
functionDisplay() {
	$MENUBOX --title "[ M A I N  ME N U ]" --menu " Use UP/DOWN Arrows to move and select or the number of your choice and enter" 15 45 4 1 "Display Start" 2 "Display stop" 3 "Display halt" X "Exit" 2>choice.txt
}

# function declaration  stop
# Script start
functionDisplay

case "`cat choice.txt`" in
	1) echo "Start";;
	2) echo "Stop";;
	3) echo "Halt";;
	X) echo "EXIT";;
esac
# script -stop
