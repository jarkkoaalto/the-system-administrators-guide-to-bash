#!/bin/bash

# Want to dispaly simple information box for out end user prior to 
# execute command. 
# Accept one command line parameter when executing the script. 
# This box should use the dialog control as shows in the course
# and display for a total of 5 second.

INFOBOX=${INFOBX=dialog}
TITLE="Default"
MESSAGE="Do Something"
XCOORD=10
YCOORD=20

funcDisplay(){
	$INFOBOX --title "$1" --infobox "$2" "$3" "$4"
	sleep "$5"
}

if [ "$1" == "shutdown" ]; then
	funcDisplay "WARNING" "we are shutting down the system..." "11" "21" "5"
	echo "Shutting Down!"
else
	funcDisplay "Information..." "You are not doing anyting fun .." "11" "21" "5"
	echo "Not doing anything..."
fi



