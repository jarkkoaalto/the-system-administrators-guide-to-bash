#!/bin/bash
# Override/trap the system exit and execute a custom function
# Global variable

TMPFILE="tmpfile.txt"
TMPFILE2="tmpfile2.txt"

trap 'functionExit' EXIT

# function declaration start
# run tihs exit instead of the default exit when called
functionExit(){
	echo "Exit Intercepted ..."
	echo "Cleaning up the temp files ..."
	rm -rf "tmpfil*.txt"
	exit 255
}
# function declaration - stop
#script start

echo "Write something to tmp file for later use..." > $TMPFILE
echo "Write something ti tmp file 2 for later use ..." > $TMPFILE2

echo "Trying to copy the indicated file before processing ..."
cp -rf $1 newfile.txt 2> /dev/null

if [ "$?" -eq "0" ]; then
	echo "Everything worked out ok ..."
else
	echo "I guess it did not work out ok ..."
	exit 1
fi
# Script stop
