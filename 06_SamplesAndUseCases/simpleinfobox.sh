#!/bin/bash
# demo of a simple info box with dialog an d ncurses

# global variables / deafult falues
INFOBOX=${INFOBOX=dialog}
TITLE="Default"
MESSAGE="Something to say"
XCOORD=10
YCOORD=20

# function declarations start

#Display the infobox and our message
funcDisplayInfoBox(){
	$INFOBOX --title "$1" --infobox "$2" "$3" "$4"
	sleep "$5"
}

# Function declarations - stop

#script start
if [ "$1" == "shutdown" ]; then
	funcDisplayInfoBox "WARNING!" "We are shutting down the system..." "11" "21" "5"
	echo "Shutting down!"
else
	funcDisplayInfoBox "Information ... " "You are not doing anything fun..." "11" "21" "5"
	echo "Not doing anything..."
fi

# script -stop

