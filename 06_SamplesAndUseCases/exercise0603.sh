#!/bin/bash

INPUTBOX=${INPUTBOX=dialog}
TITLE="default"
MESSSAGE="something to display"
XCOORD=10
YCOORD=20

funcDisplay(){
	$INPUTBOX --title "$1" --inputbox "$2" "$3" "$4" 2>tmpfile.txt
}

funcDisplay "Display File Name" "Which file in the current directory do want to display" "10" "20"

if [ "`cat tmpfile.txt`" != "" ]; then
	cat "`cat tmpfile.txt`"
else
	echo "Nothing to do"
fi
