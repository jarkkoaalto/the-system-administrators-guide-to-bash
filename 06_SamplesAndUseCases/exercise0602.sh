#!/bin/bash

# We want to display message box for our end users prior to executing command.
# Accept one command line parameter when executing the script
# This box should use the dialog control as shows in the souece and display until the ok button is clicked.
# The title and message in the box should be passed into the function but can be whatever you like will
# warn the user if the parameter passed in was "shutdown" otherwise and innicuous message can be displayed.

MSGBOX=${MSGBOX=dialog}
TITLE="default"
MESSAGE="Some Message"
XCOORD=10
YCOORD=20

funcDisplay(){
	$MSGBOX --title "$1" --msgbox "$2" "$3" "$4"
}

if [ "$1" == "shutdown" ]; then
	funcDisplay "WARNING" "Please press ok when you are ready shut down the system" "10" "20"
	echo "Shuttong down now"
else
	funcDisplay "Booring..." "You are not asking anything..." "10" "20"
	echo "Not doing anything, back to regular scripting"
fi
