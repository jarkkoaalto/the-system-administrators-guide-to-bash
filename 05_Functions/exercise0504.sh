#!/bin/bash

# We are going to use nested functions to simulate the kind of abstraction you find in many
# object oriented languages. Create the following structures in you script
# - a function that defines two variabless to hole the number of arms and legs that human being has
# - nested function one of each a male and wemale, that  contains appropriate number of that each gender has
# - capture the gender as a commandline parameter
# - test the command line parameter and call the appropriate function on order to display the characterics of thindicates gender

GENDER=$1
functionHuman() {
	ARMS=2
	LEGS=2
	echo "A human has $ARMS arms and $LEGS legs - but what gender are we?"
	echo ""

	funcMale() {
		BEARD=1
		echo "This man has $ARMS arms  and $LEGS legs, with $BEARD beard"
		echo ""
	}
	funcFemale() {
		BEARD=0
		echo "This female has $ARMS arms and $LEGS legs, with $BEARD beard"
	}
}

echo "Determining the characteristics of the gender $GENDER"
echo ""

if [ "$GENDER" == "male" ]; then
	functionHuman
	funcMale
else
	functionHuman
	funcFemale
fi
