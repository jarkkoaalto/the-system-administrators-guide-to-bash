#!/bin/bash

#Create a script to demonstrate the visibility of variables when they are available within a script and it functions.
# Define a global variable, a function that defines a local variable and then display both
# BEFORE calling the function, call the function, then display both AFTER calling the function.

GVARIABLE="Globally Visible"

funcExample(){
	LVARIABLE="Locally Visible"
	echo "From within the function $LVARIABLE..."
}

echo "Global variable = $GVARIABLE (before the function call)"
echo "Local variable = $LVARABLE (before the function call)"
funcExample

echo " " 
echo "Functon has been called ..."
echo " "
echo "Global Variable = $GVARIABLE (after the function call)"
echo "Local variable = $LVARIABLE (after the function call)" 
