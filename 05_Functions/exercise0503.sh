#!/bin/bash

# Write script that takes a single command line parameter intended to be the user's first name.
# Prompt the user for their age and read that into a variable. Using th appropriate method to make
# that command line parameter visible to a function, pass the age captured to the function
# and display a message to the user addressing them by name and confirming their age,
# add a calculation of their age in number of days.

USERNAME=$1

funcAgeInDays() {
	echo "Hi, $USERNAME, you are $1 Years old."
	echo "That makes you approximately `expr $1 \* 365` days old" 
}

echo "Enter your age"
read USERAGE

funcAgeInDays $USERAGE
