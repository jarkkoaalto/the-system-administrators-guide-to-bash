#!/bin/bash

# Demo of nested functions and some abstraction

# global variable
GENDER=$1

# function definitions -start

# create a human begin
funcHuman(){
ARMS=2
LEGS=2

echo "A Human has $ARMS arms and $LEGS legs - but what gender are we?"

 funcMale() {
	BEARD=1
	echo "This man has $ARMS arms and $LEGS legs, with $BEARD beard(s) ..."
	echo ""	
	}
 funcFemale(){
	BEARD=0
	echo "This woman has $ARMS arms and $LEGS legs, with $BEARD beard(s) ..."
	echo ""
	}
}

# Functiob definitions - stop

# Script - start
clear
echo "Determining the characteristics of the gender $GENDER"
echo ""

# determine the actual gender and display the characteristic
if [ "$GENDER" == "male" ]; then
	funcHuman
	funcMale
else
	funcHuman
	funcFemale
fi
