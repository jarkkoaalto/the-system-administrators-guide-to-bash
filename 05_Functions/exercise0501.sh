#!/bin/bash

# Create simple script containing a single function. This function should display message to clearly indicate
# it was generate when the function was run. Then display another message the function clearly 
# indicating it was generated outside of it.

echo "Starting the funcyion definition ..."

funcExample() {
	echo "We are now INSIDE the function ..."
}

echo "But we did Not call the function ..."

funcExample
echo "Now we coll function"

