#!/bin/bash

# demostrationg variable scope

# global variable declaration
GLOBALVAR="Globally Visible"

#Function definitions -start

# sample function for function variable scorpe
funcExample () {
	# local variable to the function
	LOCALVAR="Locally Visible"
	echo "From with the function, the variable is $LOCALVAR ..."
}

# function definitions -stop

#script start
clear

echo "This step happens first ..."
echo " "
echo "GLOBAL variable = $GLOBALVAR (before the function call)"
echo "LOCAL variable = $LOCALVAR (before the function call)"
echo ""
echo "Calling Function - funcExample()"
echo ""

funcExample

echo ""
echo "Function has been called.."
echo ""
echo "GLOBAL variable = $GLOBALVAR (after the function call)"
echo "LOCAL variable = $LOCALVAR (after the function call)"
