#!/bin/bash

# test multible expressions in single if statement

FILENAME=$1

echo "Testing for file $FILENAME and readability"

if [ -f $FILENAME ] && [ -r $FILENAME ]
    then
	echo "File $FILENAME exists AND is readable"
else
	echo "File $FILENAME is not exist or readable"
fi
