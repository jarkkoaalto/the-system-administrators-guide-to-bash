#!/bin/bash

# demo of the case statement
clear

echo "MAIN MENU"
echo "========="

echo "1) Choise One"
echo "2) Choise Two"
echo "3) Choise Three"
echo ""
echo "Enter Choise: "
read MENUCHOICE

case $MENUCHOICE in
    1)
	echo "Concratulation for Coosing the First Oprions";;
    2) 
	echo "Choise 2 Chosen";;
    3)
	echo "Last Choise Made";;
    *) 
	echo "You chose unwisely";;
esac
