#!/bin/bash

# if then else and nested if statement example

echo "Enter a number between 1 to 3"
read VALUE

if [ "$VALUE" -eq "1" ] 2>/dev/null; then
    echo "You give number #1"
elif [ "$VALUE" -eq "2" ] 2>/dev/null; then
    echo "You entered number #2"
elif [ "$VALUE" -eq "3" ] 2>/dev/null; then
    echo "You entered number #3"
else
    echo "You didn't follow the directions"
fi
