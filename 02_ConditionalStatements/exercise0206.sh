#!/bin/bash

# While loop exercise

echo "Enter number of times to display "Hi there" message"
read DM

COUNT=1

while [ "$COUNT" -le "$DM" ]
do
    echo "Hi There - $COUNT"
    COUNT="`expr $COUNT + 1`"
done
