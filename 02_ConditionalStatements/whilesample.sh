#!/bin/bash

# While loop example

echo "Enter the number of times to display the 'Hello World' message"
read DNUMBER

COUNT=1

while [ $COUNT -le $DNUMBER ]
do
    echo "Hello World - $COUNT"
    COUNT="`expr $COUNT + 1`"
done
