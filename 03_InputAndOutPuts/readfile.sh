#!/bin/bash

# exmple reading file (superheroes.txt)  and displaying one line at the time

echo "Enter a filename to read: "
read FILE

while read -r SUPERHERO; 
do 
    echo "Superhero Name: $SUPERHERO"
done < "$FILE"
