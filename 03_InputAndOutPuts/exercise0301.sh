#!/bin/bash

# Simple example file reading and displaying one line at the time

echo "Enter a filename to read: "
read FILE

while read -r READFILE;
do
   echo "Reading file $READFILE"
done<"$FILE"
