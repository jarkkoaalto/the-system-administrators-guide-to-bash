#!/bin/bash

# Delimiter example using IFS

# Tarkoitaa sitä, että luetaan tiedosto jossa välimerkkillä on eroitettu tiedot, kuten .csv " " tai muu välimerkki , ; , 

echo "Enter filename to parse: "
read FILE

echo "Enter the Delimeter: "
read DELIM

IFS="$DELIM"

while read -r CPU MEMORY DISK; 
do
   echo "CPU: $CPU"
   echo "Memory: $MEMORY"
   echo "Disk: $DISK"
done<"$FILE"
