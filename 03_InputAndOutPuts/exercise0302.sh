#!/bin/bash

# Demo of reading and writing to a file using a file description

echo "Enter a file name to read: "
read FILE

exec 5<>$FILE

while read -r READFILE;
do
    echo "Reading file : $READFILE"
done<&5

echo "File Was Read on:`date`">&5
exec 5>&-
