#!/bin/bash

# Write a script intended to iterate through an array called SERVERLIST containing least four values
# Display all four values to the terminal

SERVERLIST=("Webserver01" "Webserver02" "Webserver03" "Webserver04")
COUNT=0

for INDEX in ${SERVERLIST[@]}; do
	echo "Processing Servers : ${SERVERLIST[COUNT]}"
	COUNT="`expr $COUNT + 1`"
done
