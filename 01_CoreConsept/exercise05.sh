#!/bin/bash

# write a script that run: evaluate arithmetic expression, 
# attempt to remove file that does exit and evaluate aritmetic expression.
clear
 set -e

expr 1 + 3
echo $?

rm ghost.sh
echo $?
# Script stop this place a error if we use set -e 

expr 10 + 10
echo $?
