#!/bin/bash

# using declare command changing variables int to string and string to int

NEWVAR=111
echo $NEWVAR # 111

declare +i NEWVAR # declare int
declare -p NEWVAR # declare  -- NEWVAR == "111"

NEWVAR="Something"
echo $NEWVAR

declare -r READONLY="This is a string we cannot owerwrite"
declare -p READONLY 
## declare -r READONLY="this is a string we cannot owerwrite"
READONLY="New Value" # READONLY: readonly variable
declare +r READONLY ## Cannot change this variable

readonly MYREADONLY="This is String"
declare -p MYREADONLY
MYREADONLY="Something new"
declare +r MYREADONLY ## declare READONLY: readonlu variable

