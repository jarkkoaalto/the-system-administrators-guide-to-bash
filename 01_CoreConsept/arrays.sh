#!/bin/bash

# examples
MYARRAY=("First" "Second" "third")
echo ${MYARRAY[0]}
echo ${MYARRAY[1]}
echo ${MYARRAY[2]}

echo ${MYARRAY[*]}

MYARRAY[3]="Fourth"
echo ${MYARRAY[*]}

NEWARRAY=("first","second","third") #This is wrong way add value to the array
echo $NEWARRAY # first,second,third

NEWARRAY=("New Value" "Second new" "Third Value")
echo ${NEWARRAY[*]}
echo ${NEWARRAY[2]} ## Third Array

####
#simple array list and loop for display
echo ""
echo""
SERVERLIST=("webserver01" "webserver02" "webserver03" "webserver04")
COUNT=0

for INDEX in ${SERVERLIST[@]}; do
    echo "Processing Server: ${SERVERLIST[COUNT]}"
    COUNT="`expr $COUNT + 1`"
done
