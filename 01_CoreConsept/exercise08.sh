#!/bin/bash

#Create script that interact with the user. You will want to promt the user to enter
# the following information firstname, lastname, userage
# Greet the user with their name and current age displayed and the calculate and display their age in 10 years

echo "Enter Your First Name"
read FIRSTNAME
echo "Enter Your Last Name"
read LASTNAME
echo ""
echo "Your Full Name is : $FIRSTNAME  $LASTNAME"
echo ""
echo "Enter your age:"
read AGE
echo ""
echo "In 10 Year, you will be `expr $AGE + 10` years old"
