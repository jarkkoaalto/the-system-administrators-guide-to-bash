#!/bin/bash
clear
echo "This script will give us environment information"
echo "================================================"
echo ""
echo "Hello Username: $USER"
echo ""
echo "Your Home Directory is $HOME"
echo ""
echo "Your History Fill Will Ignore : $HISTCONTROL"
echo ""
echo "Your Terminal Session Type is: $TERM"
echo ""
echo "Your OS Language is : $LANG"
