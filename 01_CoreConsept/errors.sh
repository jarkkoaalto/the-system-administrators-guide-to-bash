#!/bin/bash
# This is to show exit status types

set -e # Error stop program running
man set >> "setman.txt"

expr 1 + 5
echo $?

rm doodles.sh
echo $?

expr 10 + 10
echo $?
