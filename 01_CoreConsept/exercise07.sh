#!/bin/bash

# Write script that will use command substitution to dynamically set variable values
# TODAYSDATE
# USERFILES
# and additionally, set two aliases
# TODAY
# UFILES
# Finally display all variables and aliases values when script is run.

shopt -s expand_aliases

alias TODAY="day"
alias UFILES="find /home -user jarkko"

TODAYSDATE=`date`
USERFILES=`find \home -user jarkko`

echo "Today's Date: $TODAYSDATE"
echo "All users home files: $USERFILES"

D=`TODAY`
U=`UFILES`

echo "Aliases, TODAY is: $D"
echo "Aliases, UFILES is: $U"

