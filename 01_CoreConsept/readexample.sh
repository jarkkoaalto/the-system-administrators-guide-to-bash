#!/bin/bash

# Interactive script for user input

echo "Enter Your First Name: "
read FIRSTNAME
echo "Enter Your Last Name: "
read LASTNAME

echo ""
echo "Your Full Name is: $FIRSTNAME $LASTNAME"
echo ""
echo "Enter your age :"
read AGE

echo "In 10 Years, Your will be `expr $AGE + 10` years old."
