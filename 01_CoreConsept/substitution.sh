#!/bin/bash

# This script is intended to show to do simple substitution

# Fist and easiest way to do this
#================================= 
TODAYSDATE=`date`
USERFILES=`find /home -user jarkko`


# Second and hard way to do this
#================================
shopt -s expand_aliases # shopt command give permission use aliases

man shopt >> "shopt.txt" 


alias TODAY="date"
alias UFILES="find /home -user jarkko"


TODAYSDATE=`date`
USERFILES=`find /home -user jarkko`

echo "Today's Date : $TODAYSDATE"
echo "All files Owned by user :  $USERFILES"

A=`TODAY`
B=`UFILES` 
echo "With Alias, Today is : $A"
echo "With Alias, ufiles is : $B"
